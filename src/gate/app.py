from flask import Flask, request, Response

import requests


_DOCKER_NETWORK_NAME = 'calculator-network'
_ADD_SERVICE_NAME = 'add'
_SUB_SERVICE_NAME = 'subtract'
_FLASK_SERVICE_DEFAULT_PORT = '5000'
_OPERATION_TO_SERVICE_URL = {
    'add': f'http://{_ADD_SERVICE_NAME}:{_FLASK_SERVICE_DEFAULT_PORT}/',
    'sub': f'http://{_SUB_SERVICE_NAME}:{_FLASK_SERVICE_DEFAULT_PORT}/',
}


def create_app():
    app = Flask(__name__)

    @app.route("/", methods=['GET'])
    def calculate() -> Response:
        op = request.args.get('operation')
        params = {
            'arg1': request.args.get('arg1'),
            'arg2': request.args.get('arg2'),
        }
        return requests.get(url=_OPERATION_TO_SERVICE_URL[op], params=params)

    return app


if __name__ == '__main__':
    gate_app = create_app()
    gate_app.run(host='0.0.0.0', debug=True)
